#####################################
#
# Name: KAG Signature Generator
# Autor: Bartosz St�pie�
# Contact: bartez119@gmail.com
# Autor Homepage: http://bartez.tumblr.com
# Version: 1.0
#
# License: FREE FOR NON COMERCIAL USE
#
#####################################

#REQUIREMENTS
PHP 5.2 or higher with GD & JSON library

1. Polish Version
 - Jak u�ywa�
 - W�asne modyfikacje
2. English Version
 - How to use
 - Own modification


#Polish Version:
Skrypt, kt�ry generuje sygnatur� gracza za pomoc� API dostarczanych przez deweloper�w. Skryptu mo�na u�ywa� do cel�w niekomercyjnych. Zakaz sprzedawania,
rozpowszechniania skryptu bez zgody autora. Najnowsz� wersj� skryptu mo�na pobra� na: https://bitbucket.org/bartez119/kag-signature/

Skrypt mo�na dobrowolnie modyfikowa�.

#JAK U�YWA�
Obrazek generuje si� gdy podasz wszystkie potrzebne parametry np.: http://generator.kag2d.pl/syg.php?bg=1&player=bartez&country=pl&font=coolveticarg&rgb=255,255,123
gdzie:
	bg - numer obrazka t�a.
	player - nazwa gracza
	country - kraj, International Standard ISO-3166-1993
	font - nazwa czcionki
	rgb - kolory napis�w oddzielone przecinkami(Czerwony,Zielony,Niebieski)

#W�ASNE MODYFIKACJE
W�asne obrazki t�a nale�y doda� do folderu bg. Obrazki musz� by� w formacie PNG. Czcionki nale�y doda� do folderu font, musz� one by� w formacie TTF.  	

#English Version
Script which generate a player signature from KAG API. You can use script for non commercal purposes. Prohibition of sale,dissemination of the script 
without author permission. The latest version of the script can be downloaded at: https://bitbucket.org/bartez119/kag-signature/

#HOW TO USE
Generates a picture when you give all the necessary parameters such as: http://generator.kag2d.pl/syg.php?bg=1&player=bartez&country=pl&font=coolveticarg&rgb=255, 255.123
where:
bg - the number of background image.
player - the player's name
country - a country, the International Standard ISO-3166-1993
font - font name
rgb - the colors of strings separated by commas (Red, Green, Blue)
