<?
#####################################
#
# Name: KAG Signature Generator
# Filename: syg.class.php
# Autor: Bartosz St�pie�
# Contact: bartez119@gmail.com
# Autor Homepage: http://bartez.tumblr.com
# Version: 1.0
#
# License: FREE FOR NON COMERCIAL USE
#
#####################################
class syg{
    
    public $playerdata;
    public $playeravatar;
    
        public function checkUser($user){
            $this->playerdata = json_decode(file_get_contents("https://api.kag2d.com/player/".$user."/info"),true);
            $this->playeravatar = json_decode(file_get_contents("https://api.kag2d.com/player/".$user."/avatar/"),true);
            
            if(!$this->playerdata || isset($this->playerdata['statusMessage']) && $this->playerdata['statusMessage'] == "Player not found") return false;
            else return true;
        }
        
        public function createImgError($error="Player name is invalid",$font="font/coolveticarg.ttf"){
            $bg = imagecreatefrompng("bg/1.png");
            imagettftext($bg, 12, 0, 10, 34, imagecolorallocate($bg, 255, 0, 0), $font,$error);
            header("Content-type: image/png");
            imagePNG($bg);
            imagedestroy($bg);
        }
        
		
		/*Create Sygnature with default options */
        public function createSyg($background ="bg/1.png",$colors = array(
            'text' => array(255,255,123), 
            'text-shadow' => array(8,0,0),
            'inactive' => array(192,192,192),
            'active' => array(0, 236, 0),
            'banned' => array(255, 0, 0),
            'gold-player' => array(236, 211, 0),
            'kag-guard' => array(0, 236, 0),
            'kag-team' => array(183, 0, 236)
        ),$position = array(
            'nick' => array(49,19),
            'nick-value' => array(103,19),
            'status' => array(49,32),
            'status-value' => array(103,32),
            'rank' => array(49,45),
            'rank-value' => array(89,45)
        ),$avatar = array(
            'x' => 10,
            'y' => 10,
            'size-x' => 36,
            'size-y' => 36
        ),$flag = array(
            'image' => 'flags/pl.gif',
            'x' => 85,
            'y' => 9,
            'size-x' => 16,
            'size-y' => 11
        ),$font = array(
            'font' => 'font/coolveticarg.ttf',
            'size' => 12,
            'angle' => 0
        ),$text_shadow=1){
            //CODE :)
            
            $image = imagecreatefrompng($background); //CREATE IMAGE BG
            $flagimg = imagecreatefromgif($flag['image']); //CREATE COUNTRY FLAG
            imagecopy($image, $flagimg, $flag['x'], $flag['y'], 0, 0, $flag['size-x'], $flag['size-y']); //COPY FLAG TO IMAGE
            
				//CREATE DEFAULT AVATAR IF NOT AVAIBLE
				
                 imagecopyresized($image,  imagecreatefrompng("images/Knight.png"),$avatar['x'],$avatar['y'],0,0,$avatar['size-x'],$avatar['size-y'],100,100);     
             
			 //CHECK AVATAR
			 if($this->playeravatar){
                 $playeravatar_size = getimagesize($this->playeravatar['medium']);
                if($playeravatar_size['mime'] == "image/jpeg"){
                    $avatarimg = imagecreatefromjpeg($this->playeravatar['medium']);
                }elseif($playeravatar_size['mime'] == "image/png"){
                    $avatarimg  = imagecreatefrompng($this->playeravatar['medium']);
                }elseif($playeravatar_size['mime'] == "image/gif"){
                    $avatarimg  = imagecreatefromgif($this->playeravatar['medium']);
                }
                
				//COPY AVATAR TO IMAGE
                @imagecopyresized($image, $avatarimg, $avatar['x'], $avatar['y'], 0,0, $avatar['size-x'], $avatar['size-y'], 96, 96);
             }
             
           
           //USER RANKS:
        
            if($this->playerdata['gold'] == 1 && $this->playerdata['role'] == 0 || $this->playerdata['gold'] == 1 && $this->playerdata['role'] == 5){
                if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['rank-value'][0]+1, $position['rank-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Gold Player");
                imagettftext($image, $font['size'], $font['angle'], $position['rank-value'][0], $position['rank-value'][1],imagecolorallocate($image,$colors['gold-player'][0], $colors['gold-player'][1], $colors['gold-player'][2]), $font['font'], "Gold Player");
                                
            }elseif($this->playerdata['role'] == 2 ){
                if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['rank-value'][0]+1, $position['rank-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "KAG Guard");
                imagettftext($image, $font['size'], $font['angle'], $position['rank-value'][0], $position['rank-value'][1], imagecolorallocate($image,$colors['kag-guard'][0], $colors['kag-guard'][1], $colors['kag-guard'][2]), $font['font'], "KAG Guard");
                
            }elseif($this->playerdata['role'] == 1 || $this->playerdata['role'] == 4){
                if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['rank-value'][0]+1, $position['rank-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "KAG Team");
                imagettftext($image, $font['size'], $font['angle'], $position['rank-value'][0], $position['rank-value'][1],imagecolorallocate($image,$colors['kag-team'][0], $colors['kag-team'][1], $colors['kag-team'][2]), $font['font'], "KAG Team");
            }else{
                 if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['rank-value'][0]+1, $position['rank-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Normal Player");
                 imagettftext($image, $font['size'], $font['angle'], $position['rank-value'][0], $position['rank-value'][1],imagecolorallocate($image,$colors['text'][0], $colors['text'][1], $colors['text'][2]), $font['font'], "Normal Player");
            }
            
            //CHECK STATUS:
            
            if($this->playerdata['active'] == 1 && $this->playerdata['banned'] != 1){
                if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['status-value'][0]+1, $position['status-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Active");
                imagettftext($image, $font['size'], $font['angle'], $position['status-value'][0], $position['status-value'][1],imagecolorallocate($image,$colors['active'][0], $colors['active'][1], $colors['active'][2]), $font['font'], "Active");

            }elseif($this->playerdata['active'] == 0 && $this->playerdata['banned'] != 1){
               if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['status-value'][0]+1, $position['status-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Inactive");
               imagettftext($image, $font['size'], $font['angle'], $position['status-value'][0], $position['status-value'][1],imagecolorallocate($image,$colors['inactive'][0], $colors['inactive'][1], $colors['inactive'][2]), $font['font'], "Inactive");

            }elseif ($this->playerdata['banned'] == 1) {
                  if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['status-value'][0]+1, $position['status-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Banned");
                  imagettftext($image, $font['size'], $font['angle'], $position['status-value'][0], $position['status-value'][1],imagecolorallocate($image,$colors['banned'][0], $colors['banned'][1], $colors['banned'][2]), $font['font'], "Banned");
 
            }
            
            //CAPTIONS:
            if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['nick'][0]+1, $position['nick'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'], "Nick:");
            imagettftext($image, $font['size'], $font['angle'], $position['nick'][0], $position['nick'][1],imagecolorallocate($image,$colors['text'][0], $colors['text'][1], $colors['text'][2]), $font['font'], "Nick:");
            
            if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['nick-value'][0]+1, $position['nick-value'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'],$this->playerdata['username']);
            imagettftext($image, $font['size'], $font['angle'], $position['nick-value'][0], $position['nick-value'][1],imagecolorallocate($image,$colors['text'][0], $colors['text'][1], $colors['text'][2]), $font['font'],$this->playerdata['username']);

             if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['status'][0]+1, $position['status'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'],"Status:");
             imagettftext($image, $font['size'], $font['angle'], $position['status'][0], $position['status'][1],imagecolorallocate($image,$colors['text'][0], $colors['text'][1], $colors['text'][2]), $font['font'],"Status:");

             if($text_shadow == 1) imagettftext($image, $font['size'], $font['angle'],$position['rank'][0]+1, $position['rank'][1]+1, imagecolorallocate($image,$colors['text-shadow'][0], $colors['text-shadow'][1], $colors['text-shadow'][2]), $font['font'],"Rank:");
             imagettftext($image, $font['size'], $font['angle'], $position['rank'][0], $position['rank'][1],imagecolorallocate($image,$colors['text'][0], $colors['text'][1], $colors['text'][2]), $font['font'],"Rank:");

			 //DISPLAY IMAGE
             header("Content-type: image/png");
             imagePNG($image);
             imagedestroy($image);
        }
}

?>
