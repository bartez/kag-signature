<?

#####################################
#
# Name: KAG Signature Generator
# Filename: syg.php
# Autor: Bartosz St�pie�
# Contact: bartez119@gmail.com
# Autor Homepage: http://bartez.tumblr.com
# Version: 1.0
#
# License: FREE FOR NON COMERCIAL USE
#
#####################################

require_once 'syg.class.php';

$sygnatura = new syg(); //CREATE NEW OBJECT

//CHECK IF ISSET ALL $_GET

if(isset($_GET['player']) && $_GET['player'] != ""){
    if($sygnatura->checkuser(trim($_GET['player'])) == true){
        
        if(isset($_GET['bg']) && $_GET['bg'] != ""){
            
            if(isset($_GET['country']) && $_GET['country'] != ""){
                
                if(isset($_GET['font']) && $_GET['font'] != ""){
                    if(isset($_GET['rgb']) && $_GET['rgb'] != ""){
                         $rgb = explode(",", $_GET['rgb']);
                    }else{
                        $rgb = array(255,255,123);
                    }
					
					//SWITCH background
                        switch($_GET['bg']){

                            default:
                                $background ="bg/".$_GET['bg'].".png"; //location bg image

                                $colors = array( //define colors
                                'text' => array($rgb[0],$rgb[1],$rgb[2]), //text-color
                                'text-shadow' => array(8,0,0), //text shadow color
                                'inactive' => array(192,192,192), //inactive player color
                                'active' => array(0, 236, 0), //active player color
                                'banned' => array(255, 0, 0), //banned player color
                                'gold-player' => array(236, 211, 0), //gold player 
                                'kag-guard' => array(0, 236, 0), //kag guard color
                                'kag-team' => array(183, 0, 236)); //kag team color


                                $position = array( //positions text (x,y)
                                'nick' => array(49,19), //nick position
                                'nick-value' => array(103,19), //value nick position
                                'status' => array(49,32), //status position
                                'status-value' => array(103,32), //status value position
                                'rank' => array(49,45), //rank position
                                'rank-value' => array(89,45)); //rank value position

                                $avatar = array( //avatar
                                'x' => 10, //x-position
                                'y' => 10, //y-position
                                'size-x' => 36, //width
                                'size-y' => 36); //height

                                $flag = array( //flags
                                'image' => 'flags/'.$_GET['country'].'.gif', //flags location
                                'x' => 85, //x-position
                                'y' => 9, //y-position
                                'size-x' => 16, //width
                                'size-y' => 11); //height

                                $font = array( //font
                                'font' => 'font/'.$_GET['font'].'.ttf', //font location
                                'size' => 12, //font size
                                'angle' => 0); //angle

                                $text_shadow=1; //set text shadow: 1 = ON, 0 = OFF
                            break;
							
							//ADD MORE OPTIONS BELOW:
							
							//END MORE OPTIONS.
                        }
                    
					//CREATE SIGNATURE
                    $sygnatura->createSyg($background,$colors,$position,$avatar,$flag,$font,$text_shadow);
                    
                }else $sygnatura->createImgError("Font is not set!");
                
            }else $sygnatura->createImgError("Country is not set!");
            
        }  else $sygnatura->createImgError("Background is not set!");
     
        
    }else{
        $sygnatura->createImgError();
    }
}else{
    $sygnatura->createImgError();
}